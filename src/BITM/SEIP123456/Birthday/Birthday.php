<?php
namespace App\Birthday;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;


class Birthday extends DB{
    public $id= "";
    public $name= "";
    public $birth_date= "";


    public function __construct(){
        parent::__construct();
        if (!isset($_SESSION)) session_start();
    }

    public function setData($post = NULL){
        if (array_key_exists('id', $post)){
            $this->id= $post['id'];
        }
        if (array_key_exists('name', $post)){
            $this->name = $post['name'];
        }
  if (array_key_exists('birth_date', $post)){
      $this->birth_date = $post['birth_date'];
  }

    }
    public function store(){
        $arrData = array( $this->name, $this->birth_date);

        $sql = "Insert INTO birthday(name, birth_date) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');

    }

}
